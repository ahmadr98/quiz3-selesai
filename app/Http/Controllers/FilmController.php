<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class FilmController extends Controller
{
    
    public function erd(){
        return view('konten.erd');
    }

    public function create(){
        return view('konten.create');
    }

    public function store(Request $request) {
        //dd($request->all());
        $request->validate([
            'judul' => 'required|unique:film',
            'desc' => 'required',
            'genre' => 'required'
        ]);
        $query = DB::table('film')->insert([
            "judul" => $request["judul"],
            "desc" => $request["desc"],
            "genre" => $request["genre"]
            ]);
            return redirect('/film')->with('success', 'Berhasil di simpan!');
            
    }

    public function index() {
        $posts = DB::table('film')->get();
        //dd($posts);
        return view('konten.index', compact('posts'));
    }

    public function show($id){
        $post = DB::table('film')->where('id',$id)->first();
        //dd($post);
        return view('konten.show', compact('post'));
    }

    public function edit($id){
        $post = DB::table('film')->where('id', $id)->first();

        return view('konten.edit', compact('post'));
    }
    
    public function update($id, Request $request){
        $request->validate([
            'judul' => 'required|unique:film',
            'desc' => 'required',
            'genre' => 'required'
        ]);

        $query = DB::table('film')
                    ->where('id', $id)
                    ->update([
                        'judul' => $request['judul'],
                        'desc' => $request['desc'],
                        'genre' => $request['genre']
                    ]);

                    return redirect('/film') ->with('success','berhasil update!');

    }

    public function destroy($id){
        $query = DB::table('film')-> where('id',$id)->delete();
        return redirect('/film')->with('success','Berhasil di hapus!');
    }
}
