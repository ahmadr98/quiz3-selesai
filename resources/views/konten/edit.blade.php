@extends('layouts.master')
@section('content')

<div class="container">
    <div class="card mt-4" style="width: 25rem;">
        
        <div class="card-body">
          <h5 class="card-title">Edit {{$post->id}}</h5><br><br>
<form role="form" action="/film/{{$post->id}}" method="POST" >
    @csrf
    @method('PUT')
    <div class="mb-3">
      <label for="judul" class="form-label">Judul</label>
      <input type="text" class="form-control" id="judul" name="judul" value=" {{ old ('judul', $post->judul) }} " placeholder="Isi judul" required>
      @error('judul')
      <div class="alert alert-danger">{{ $message}}</div>
      @enderror
    </div>
    <div class="mb-3">
      <label for="isi" class="form-label">Isi Pertanyaan</label>
      <input type="text" class="form-control" id="desc" name="desc" value="{{ old('desc', $post->desc)}}" placeholder="isi pertanyaan" required>
      @error('desc')
      <div class="alert alert-danger">{{ $message}}</div>
      @enderror
    </div>
    <div class="mb-3">
        <label for="genre" class="form-label">genre</label>
        <input type="text" class="form-control" id="genre" name="genre" value="{{ old('genre', '')}}" placeholder="desc" required>
        @error('genre')
        <div class="alert alert-danger">{{ $message}}</div>
        @enderror
      </div>
    
    <button type="submit" class="btn btn-primary">Edit</button>
  </form>
</div>
    </div>
</div>

@endsection