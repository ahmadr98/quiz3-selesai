@extends('layouts.master')
@section('content')
<html lang="en">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    
<body>
    <div class="container pt-5">
        <div class="card ">
            @if(session('success'))
            <div class="alert alert-success">
                {{session('success')}}
            </div>
            @endif
          <a  class="btn btn-primary" href ="/film/create"> Film baru </a>
        <table class="table table-striped table-bordered my-3 ">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Judul</th>
                <th scope="col">Desc</th>
                <th scope="col">genre</th>
                <th scope="col">Aksi</th>
              </tr>
            </thead>
            <tbody>
                @foreach($posts as $key => $post)
                <tr>
                    <td> {{ $key + 1 }} </td>
                    <td> {{ $post->judul}} </td>
                    <td> {{ $post->desc}} </td>
                    <td> {{ $post->genre}} </td>

                    <td  style="display:flex;">
                        <a href="/film/{{$post->id}}" class="btn btn-info btn-sm ">show</a>
                        <a href="/film/{{$post->id}}/edit" class="btn btn-success btn-sm">edit</a>
                        <form action="/film/{{$post->id}}" method="post">
                          @csrf
                          @method('DELETE')
                        <input type="submit" value="delete" class="btn btn-danger btn-sm">
                        </form>

                         </td>
                </tr>
             @endforeach
            </tbody>
          </table>
        </div>
      </div>
    
      
    
</body>
</html>
@endsection